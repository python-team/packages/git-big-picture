git-big-picture (1.3.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control
    - Bump Standards-Version to 4.6.2.
    - Switch Testsuite from autopkgtest-pkg-python to
      autopkgtest-pkg-pybuild.
  * debian/copyright
    - Bump my copyright years.
  * debian/patches/privacy-breach.patch
    - New patch; don't use remote images in README.
  * debian/tests/control
    - Drop pytest; now handled by autodep8.

 -- Doug Torrance <dtorrance@debian.org>  Fri, 08 Mar 2024 19:51:16 -0500

git-big-picture (1.2.2-1) unstable; urgency=medium

  * New upstream release.

 -- Doug Torrance <dtorrance@debian.org>  Tue, 27 Sep 2022 16:50:01 -0400

git-big-picture (1.2.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/gbp.conf: New file; set debian-branch to DEP-14 recommended
    "debian/latest".

 -- Doug Torrance <dtorrance@debian.org>  Sat, 26 Mar 2022 19:38:18 -0400

git-big-picture (1.2.0-2) unstable; urgency=medium

  * Source-only upload for transition to testing.
  * debian/control
    - Add dh-sequence-python3 to Build-Depends.
    - Add support for "nocheck" build profile by adding <!nocheck> to all
      packages in Build-Depends that are only used during tests.
  * debian/rules
    - Drop "--with python3"; not necessary now that dh-sequence-python3 is
      in Build-Depends.
  * debian/upstream/metadata
    - Add "---" for document start.

 -- Doug Torrance <dtorrance@debian.org>  Sat, 05 Mar 2022 22:03:02 -0500

git-big-picture (1.2.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/clean
    - Clean up generated README.html.
  * debian/control
    - Update my email address; now a Debian Developer.
    - Add python3-parameterized to Build-Depends; needed for a test.
    - Add python3-docutils and python3-pygments to Build-Depends for
      generating the html documentation.
    - Bump Standards-Version to 4.6.0.
    - Add Testsuite field for autodep8 test.
    - Split into two binary packages: git-big-picture for the
      command-line tool and python3-git-big-picture for the Python module.
  * debian/copyright
    - Update Sebastian Pipping's copyright years.
    - Update my copyright years and email address.
  * debian/docs
    - Install new README.html and screenshots directory.
  * debian/*.install
    - Add files to specify what goes into each binary package.
  * debian/patches
    - Remove directory; only patch was applied upstream.
  * debian/rules
    - Generate html doc from README.rst.
  * debian/tests/control
    - Split the pytest and cram tests into separate tests.
    - Add test names.

 -- Doug Torrance <dtorrance@debian.org>  Tue, 01 Mar 2022 22:47:37 -0500

git-big-picture (1.1.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/clean
    - Add test.cram.err file that is generated when cram tests fail.
  * debian/control
    - Restore python3-cram to Build-Depends; upstream switched back
      to using cram for tests.
    - Restore graphviz to Build-Depends; needed for cram tests.
  * debian/{git-big-picture.1,manpages}
    - Remove files; manpage is now shipped by upstream.
  * debian/patches/use-python3-in-cram-test.patch
    - New patch; use python3 instead of python in cram test.
  * debian/rules
    - Run cram tests after the console script has been installed.
  * debian/tests/control
    - Run cram tests during autopkgtest again.

 -- Doug Torrance <dtorrance@piedmont.edu>  Sat, 23 Jan 2021 13:17:41 -0500

git-big-picture (1.0.0-1) unstable; urgency=medium

  * New upstream release.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Doug Torrance ]
  * debian/clean
    - Add .pytest_cache directory; generated during build.
  * debian/control
    - Bump Standards-Version to 4.5.1.
    - Update Homepage.
    - Add python3-pytest to Build-Depends.
    - Remove graphviz from Build-Depends; no longer necessary.
    - Remove python3-cram from Build-Depends. Upstream has switched to
      scruf for this test, but it is not available in Debian yet.
  * debian/copyright
    - Update Source link.
    - Update years.
  * debian/git-big-picture.1
    - Update manpage.
  * debian/patches
    - Remove patches; no longer necessary.
  * debian/pybuild.testfiles
    - New file; copy test file to build directory.
  * debian/rules
    - Drop override_dh_auto_test target; instead we use pybuild for
      testing and set PYBUILD_TEST_* environment variables.
  * debian/tests/control
    - Update Test-Command; we now use pytest and not cram.
  * debian/upstream/metadata
    - Update links.
  * debian/watch
    - Update download link.

 -- Doug Torrance <dtorrance@piedmont.edu>  Thu, 14 Jan 2021 13:20:13 -0500

git-big-picture (0.10.1-1) unstable; urgency=medium

  [ Doug Torrance ]
  * New upstream release.
    - Add Python 3 support (Closes: #936615).
  * debian/control
    - Switch Maintainer to PAPT and add self to Uploaders.
    - Bump debhelper compatibility level to 13.
    - Add dh-python to Build-Depends and drop python and python-setuptools
      in favor of python3-all and python3-setuptools.
    - Add git, graphviz, and python3-cram to Build-Depends for use during
      test.
    - Switch from ${python:Depends} to ${python3:Depends}.
    - Update Vcs-* links to Salsa.
    - Remove obsolete X-Python-Version.
    - Bump Standards-Version to 4.5.0.
    - Add Rules-Requires-Root field.
  * debian/copyright
    - Use https in Format field.
  * debian/patches/cram_git_identity.patch
    - Set up fake user so we don't get fatal 'empty ident name' error
      during test.
  * debian/patches/use_python3_shebang.patch
    - Run script using Python 3.
  * debian/rules
    - Remove get-orig-source target.
    - Use pybuild and switch from python2 to python3.
    - New override_dh_auto_test target which runs upstream test.
    - Fix trailing whitespace
  * debian/salsa-ci.yml
    - Add Salsa-Pipeline config file.
  * debian/tests/control
    - Add continuous integration test.
  * debian/watch
    - Bump to uscan v4 and use special strings.

  [ Debian Janitor ]
  * debian/copyright: use spaces rather than tabs to start continuation
    lines.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Doug Torrance <dtorrance@piedmont.edu>  Mon, 11 May 2020 18:12:11 -0400

git-big-picture (0.9.0+git20131031-2) unstable; urgency=medium

  * debian/{control,copyright}
    - Update email address.

 -- Doug Torrance <dtorrance@piedmont.edu>  Sun, 23 Aug 2015 01:51:36 -0400

git-big-picture (0.9.0+git20131031-1) unstable; urgency=low

  * Initial release (Closes: #774782).

 -- Doug Torrance <dtorrance@monmouthcollege.edu>  Wed, 07 Jan 2015 14:54:32 -0600
